'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { EntityRepository, Repository, UpdateResult } from 'typeorm'
import User from '../entity/User'

@EntityRepository(User)
export default class UserRepository extends Repository<User> {
    findOneByUsernameOrEmail(user: User):Promise<User|undefined> {
        return this.createQueryBuilder('user')
            .where('user.username = :username', user)
            .orWhere('user.email = :email', user)
            .getOne()
    }

    findOneByUsernameWithPassword(user: User):Promise<User|undefined> {
        return this.createQueryBuilder('user')
            .where('user.username = :username', user)
            .addSelect('user.password')
            .getOne()
    }

    findByPk(userId: number):Promise<User|undefined> {
        return this.findOne({where: {id: userId}})
    }

    updatePassword(user: User):Promise<UpdateResult> {
        return this.createQueryBuilder('user')
            .update(User)
            .set(user)
            .where("id = :id", { id: user.id })
            .execute()
    }

    updateStock(user: User, nbToAdd: number):Promise<UpdateResult> {
        return this.createQueryBuilder('user')
            .update(User)
            .where("id = :id", { id: user.id })
            .set({ stock: () => `stock + ${nbToAdd}` })
            .execute()
    }

    decrementStock(user: User):Promise<UpdateResult> {
        return this.updateStock(user, -1)
    }
}