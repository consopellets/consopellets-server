'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { inject, injectable } from 'inversify'
import jwt, { verify } from 'jsonwebtoken'
import expressJwt, { RequestHandler } from 'express-jwt'
import config from 'config'
import User from '../entity/User'
import RefreshToken from '../entity/RefreshToken'
import DatabaseService from './DatabaseService'
import RefreshTokenRepository from '../repositories/RefreshTokenRepository'
import { Request } from 'express'

@injectable()
export default class JwtService {
    @inject(DatabaseService) private readonly databaseService!: DatabaseService

    public static verifyAccessToken(request: Request): void {
        const authorization = request.headers["authorization"]

        if (authorization === undefined) {
            throw new Error()
        }

        const token = authorization.split(' ')[1]
        request.user = verify(token, config.get('jwt.accessTokenSecret'), {algorithms: ['HS256']})
    }

    public static verifyAccesshTokenHandler(): RequestHandler {
        return expressJwt({ secret: config.get('jwt.refreshTokenSecret'), algorithms: ['HS256']})
    }

    public static verifyRefreshTokenHandler(): RequestHandler {
        return expressJwt({ secret: config.get('jwt.refreshTokenSecret'), algorithms: ['HS256']})
    }
    
    public async decodeAccessToken(accessToken: string): Promise<any> {
        return jwt.decode(accessToken)
    }
    
    public async decodeRefreshToken(refreshToken: string): Promise<any> {
        return jwt.decode(refreshToken)
    }

    public generateAccessToken(user: User): string {
        return this.generateToken(user)
    }

    /**
     * Génère un nouveau refreshToken.
     * Si un ancien refreshToken est passé en paramètre, contrôle qu'il est encore valide et
     *  le supprime de la table des refrehToken valides.
     *
     * @param {User}   user            L'utilisateur
     * @param {string} oldRefreshToken L'ancien refreshToken
     *
     * @returns {string} Le nouveau refreshToken
     *
     * @throws {RecycledRefreshTokenError} Si le refreshToken a déjà été utilisé
     */
    public async generateRefreshToken(user: User, oldRefreshToken?: string): Promise<string> {
        const refreshTokenRepo = await this.databaseService.getRepository(RefreshTokenRepository)

        if (undefined !== oldRefreshToken) {
            const oldRefreshTokenObj = await refreshTokenRepo.findByRefreshToken(oldRefreshToken)

            if (undefined === oldRefreshTokenObj) {
                throw new RecycledRefreshTokenError()
            }

            refreshTokenRepo.remove(oldRefreshTokenObj)
        }

        // Génération du nouveau refreshToken
        const refreshToken = this.generateToken(user, true)
        // On essaye d'effacer d'ancien refreshToken qui seraient périmés
        this.cleanOldRefreshToken(user)
        // Sauvegarde en base du refreshToken pour ne pouvoir l'utiliser qu'une fois
        refreshTokenRepo.save({ user, refreshToken })
        // Et on retourne le refreshToken
        return refreshToken
    }

    public refreshTokenIsStillValide(refreshToken: string): boolean {
        try {
            return jwt.verify(refreshToken, config.get('jwt.refreshTokenSecret')) !== null
        } catch(err) {
            return false
        }
    }

    private async cleanOldRefreshToken(user: User): Promise<void> {
        const refreshTokenRepo = await this.databaseService.getRepository(RefreshTokenRepository)
        const refreshTokens = await refreshTokenRepo.findLast100Created(user)
        const refreshTokensToDelete: RefreshToken[] = []

        refreshTokens.forEach((refreshToken: RefreshToken): void => {
            if (!this.refreshTokenIsStillValide(refreshToken.refreshToken)) {
                refreshTokensToDelete.push(refreshToken)
            } 
        })

        if (refreshTokensToDelete.length !== 0) {
            await refreshTokenRepo.remove(refreshTokensToDelete)
        }
    }

    private generateToken(userData: User, refreshToken: boolean = false): string {
        const tokenType = refreshToken ? 'refresh' : 'access'
        return jwt.sign({
            userId: userData.id,
            isAdmin: userData.isAdmin
        },
        config.get(`jwt.${tokenType}TokenSecret`),
        {
            expiresIn: config.get(`jwt.${tokenType}TokenExpiresIn`)
        })
    }
}

export class RecycledRefreshTokenError extends Error {}