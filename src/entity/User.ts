'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { Column, Entity, OneToMany, Unique } from 'typeorm'
import Achat from './Achat'
import Consommation from './Consommation'
import Entretient from './Entretient'
import MainEntity from './MainEntity'
import Nettoyage from './Nettoyage'
import RefreshToken from './RefreshToken'
import { IsEmail, IsNotEmpty, Length, MinLength, MinOneMajChar, MinOneMinChar, MinOneNum, MinOneSpecialChar } from '../validators/validators'
import { Validate } from 'class-validator'
import IsUserUsernameUnique from '../validators/IsUserUsernameUnique'
import IsUserEmailUnique from '../validators/IsUserEmailUnique'
import { Type } from 'class-transformer'

@Entity()
@Unique(['username'])
@Unique(['email'])
export default class User extends MainEntity {
    static readonly USERNAME_SIZE_MAX = 255
    static readonly EMAIL_SIZE_MAX = 255
    static readonly PASSWORD_SIZE_MIN = 8
    static readonly PASSWORD_SIZE_MAX = 255

    @IsNotEmpty({groups: ['register', 'login']})
    @Length(1,User.USERNAME_SIZE_MAX, {groups: ['register', 'login']})
    @Validate(IsUserUsernameUnique, {groups: ['register-conflict']})
    @Column({
        type: 'varchar',
        length: User.USERNAME_SIZE_MAX,
        nullable: false
    })
    public username!: string

    @IsNotEmpty({groups: ['register']})
    @IsEmail({}, {groups: ['register']})
    @Validate(IsUserEmailUnique, {groups: ['register-conflict']})
    @Column({
        type: 'varchar',
        length: User.EMAIL_SIZE_MAX,
        nullable: false
    })
    public email!: string

    @IsNotEmpty({groups: ['register', 'login', 'update']})
    @MinLength(8, {groups: ['register', 'update']})
    @MinOneMajChar({groups: ['register', 'update']})
    @MinOneMinChar({groups: ['register', 'update']})
    @MinOneSpecialChar({groups: ['register', 'update']})
    @MinOneNum({groups: ['register', 'update']})
    @Column({
        type: 'varchar',
        length: User.PASSWORD_SIZE_MAX,
        nullable: false,
        select: false
    })
    public password!: string

    @Column({
        type: 'boolean',
        nullable: false,
        default: false
    })
    public isAdmin!: boolean

    @Type(() => Number)
    @Column({
        type: 'integer',
        nullable: false,
        default: 0,
        unsigned: true
    })
    public stock!: number

    @OneToMany(type => RefreshToken, refreshToken => refreshToken.user)
    public refreshTokens!: RefreshToken[]

    @OneToMany(type => Achat, achat => achat.user)
    public achats!: Achat[]

    @OneToMany(type => Consommation, consommation => consommation.user)
    public consommations!: Consommation[]

    @OneToMany(type => Entretient, entretient => entretient.user)
    public entretients!: Entretient[]

    @OneToMany(type => Nettoyage, nettoyage => nettoyage.user)
    public nettoyages!: Nettoyage[]
}