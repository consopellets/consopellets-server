'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { inject, injectable } from 'inversify'
import { Authorized, Body, CurrentUser, Get, HttpCode, InternalServerError, JsonController, NotFoundError, Post } from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'
import TYPES from '../di/types'
import Achat from '../entity/Achat'
import I18n from '../interfaces/I18nInterface'
import Logger from '../interfaces/LoggerInterface'
import AchatService from '../services/AchatService'
import CleanObjectService from '../services/CleanObjectService'
import { UserNotFound } from '../services/UserService'

@OpenAPI({
    tags: ['Achats'],
    responses: {
        '401': {
            description: "L'utilisateur n'est pas autorisé",
            content: {'application/json': {schema: {$ref: "#/components/schemas/UnauthorizedUser"}}}
        },
        '500': {
            description: "**Erreur** : problème serveur",
            content: {'application/json': {schema: {$ref: "#/components/schemas/Error"}}}
        }
    }
})
@injectable()
@JsonController('/v0.1')
export default class AchatController {
    @inject(AchatService) private readonly achatService!: AchatService
    @inject(CleanObjectService) private readonly cleanObjectService! : CleanObjectService
    @inject(TYPES.I18n) private readonly i18n!: I18n
    @inject(TYPES.Logger) private readonly logger!: Logger

    @OpenAPI({
        summary: "Enregistrement achat",
        description: "Enregistrement d'un achat",
        requestBody: {
            require: true,
            description: "Les données nécessaires à l'enregistrement d'un achat",
            content: {'application/json': {schema: {$ref: '#/components/schemas/AchatAdd'}}}
        },
        responses: {
            '201': {
                description: "L'achat a bien été créé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/Achat"}}}
            },
            '400': {
                description: "**Erreur** : données invalides",
                content: {'application/json': {schema: {$ref: "#/components/schemas/DataError"}}}
            }
        }
    })
    @Authorized()
    @HttpCode(201)
    @Post('/users/me/achats')
    public async addAchatToMe(@CurrentUser({ required: true }) userJwt: any, @Body({ validate: {groups: ['add']} }) achat: Achat) {
        try {
            return this.cleanObjectService.achatWithoutUser(await this.achatService.addAchat(achat, userJwt.userId))
        } catch (err:any) {
            if (err instanceof UserNotFound) {
                throw new NotFoundError(this.i18n.t('error.users.notFound'))
            }

            this.logger.error(err)
            throw new InternalServerError(this.i18n.t('error.500'))
        }
    }

    @OpenAPI({
        summary: "Récupération de mes achats",
        description: "Récupération de mes achats",
        security: [{'accessToken': []}],
        responses: {
            '200': {
                description: "Les achats l'utilisateur",
                content: {'application/json': {schema: {$ref: "#/components/schemas/Achats"}}}
            }
        }
    })
    @Authorized()
    @Get('/users/me/achats')
    public async getMyAchats(@CurrentUser({ required: true }) userJwt: any) {
        try {
            const achats = await this.achatService.getAchatsOfUser(userJwt.userId)
            return this.cleanObjectService.achatsWithoutUser(achats)
        } catch (err: any) {
            this.logger.error(err)
            throw new InternalServerError(this.i18n.t('error.500'))
        }
    }
}