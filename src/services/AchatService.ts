'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { inject, injectable } from 'inversify'
import Achat from '../entity/Achat'
import AchatRepository from '../repositories/AchatRepository'
import UserRepository from '../repositories/UserRepository'
import DatabaseService from './DatabaseService'
import { UserNotFound } from './UserService'

@injectable()
export default class AchatService {
    @inject(DatabaseService) private readonly databaseService!: DatabaseService

    public async getAchatsOfUser(userId: number): Promise<Achat[]> {
        const achatRepo = await this.databaseService.getRepository(AchatRepository)
        return await achatRepo.getAchatsOfUser(userId)
    }

    public async addAchat(achat: Achat, userId: number): Promise<Achat> {
        const userRepo = await this.databaseService.getRepository(UserRepository)
        const achatRepo = await this.databaseService.getRepository(AchatRepository)
        const user = await userRepo.findByPk(userId)

        // Si l'utilisateur n'a pas été trouvé
        if (undefined === user) {
            throw new UserNotFound()
        }

        const dbConnection = await this.databaseService.getConnection()
        achat.user = user

        return dbConnection.transaction(async transactionalEntityManager => {
            const transactionUserRepo = transactionalEntityManager.getCustomRepository(UserRepository)
            // On met à jour le stock
            transactionUserRepo.updateStock(user, achat.quantite)
            // On enregistre l'achat et on le retourne
            return achatRepo.save(achat)
        })
    }
}