import {MigrationInterface, QueryRunner} from "typeorm";

export class createDatabase1631964628833 implements MigrationInterface {
    name = 'createDatabase1631964628833'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "username" varchar(255) NOT NULL, "email" varchar(255) NOT NULL, "password" varchar(255) NOT NULL, "isAdmin" boolean NOT NULL DEFAULT (0), CONSTRAINT "UQ_username" UNIQUE ("username"), CONSTRAINT "UQ_email" UNIQUE ("email"))`);
        await queryRunner.query(`CREATE TABLE "consommation" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "dateConsommation" date NOT NULL, "userId" integer NOT NULL, CONSTRAINT "FK_userId" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`CREATE TABLE "entretient" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "type" integer NOT NULL, "email" text NOT NULL, "prix" integer NOT NULL, "dateEntretient" date NOT NULL, "userId" integer NOT NULL, CONSTRAINT "FK_userId" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`CREATE TABLE "nettoyage" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "dateNettoyage" date NOT NULL, "userId" integer NOT NULL, CONSTRAINT "FK_userId" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`CREATE TABLE "refresh_token" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "refreshToken" varchar NOT NULL, "userId" integer NOT NULL, CONSTRAINT "FK_userId" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
        await queryRunner.query(`CREATE TABLE "achat" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "quantite" integer NOT NULL, "prix" integer NOT NULL, "dateAchat" date NOT NULL, "userId" integer NOT NULL, CONSTRAINT "FK_userId" FOREIGN KEY ("userId") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE NO ACTION)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "achat"`);
        await queryRunner.query(`DROP TABLE "refresh_token"`);
        await queryRunner.query(`DROP TABLE "nettoyage"`);
        await queryRunner.query(`DROP TABLE "entretient"`);
        await queryRunner.query(`DROP TABLE "consommation"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
