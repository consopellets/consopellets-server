'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { EntityRepository, Repository, UpdateResult } from 'typeorm'
import Achat from '../entity/Achat'

@EntityRepository(Achat)
export default class AchatRepository extends Repository<Achat> {
    getAchatsOfUser(userId: number): Promise<Achat[]> {
        return this.createQueryBuilder('achat')
            .where('achat.user = :userId', {userId: userId})
            .getMany()
    }
}