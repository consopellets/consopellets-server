'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { EntityRepository, Repository } from 'typeorm'
import RefreshToken from '../entity/RefreshToken'
import User from '../entity/User'

@EntityRepository(RefreshToken)
export default class RefreshTokenRepository extends Repository<RefreshToken> {
    public async findLast100Created(user: User):Promise<RefreshToken[]> {
        return this.createQueryBuilder('refreshToken')
            .where('refreshToken.userId = :id', user)
            .orderBy('createdAt', 'ASC')
            .limit(100)
            .getMany()
    }

    public async findByRefreshToken(refreshToken: string):Promise<RefreshToken|undefined> {
        return this.findOne({where: {refreshToken}})
    }
}