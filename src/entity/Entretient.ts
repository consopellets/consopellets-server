'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { Column, Entity, ManyToOne } from 'typeorm'
import MainEntity from './MainEntity'
import User from './User'

enum EntretientType {
    Annuel = 0,
    Ramonage = 1,
    Reparation = 2
}

@Entity()
export default class Entretient extends MainEntity {
    @ManyToOne(type => User, user => user.entretients, {nullable: false, onDelete:'CASCADE'})
    public user!: User

    @Column({
        type: 'integer',
        nullable: false
    })
    public type!: EntretientType

    @Column({
        type: 'text',
        nullable: false
    })
    public email!: string

    @Column({
        type: 'integer',
        nullable: false
    })
    public prix!: number

    @Column({
        type: 'date',
        nullable: false
    })
    public dateEntretient!: Date
}