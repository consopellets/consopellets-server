import {MigrationInterface, QueryRunner} from "typeorm";

export class ajoutDuStock1633270925935 implements MigrationInterface {
    name = 'ajoutDuStock1633270925935'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "temporary_user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "username" varchar(255) NOT NULL, "email" varchar(255) NOT NULL, "password" varchar(255) NOT NULL, "isAdmin" boolean NOT NULL DEFAULT (0), "stock" integer NOT NULL DEFAULT (0), CONSTRAINT "UQ_email" UNIQUE ("email"), CONSTRAINT "UQ_username" UNIQUE ("username"))`);
        await queryRunner.query(`INSERT INTO "temporary_user"("id", "createdAt", "updatedAt", "username", "email", "password", "isAdmin") SELECT "id", "createdAt", "updatedAt", "username", "email", "password", "isAdmin" FROM "user"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`ALTER TABLE "temporary_user" RENAME TO "user"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" RENAME TO "temporary_user"`);
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "createdAt" datetime NOT NULL DEFAULT (datetime('now')), "updatedAt" datetime NOT NULL DEFAULT (datetime('now')), "username" varchar(255) NOT NULL, "email" varchar(255) NOT NULL, "password" varchar(255) NOT NULL, "isAdmin" boolean NOT NULL DEFAULT (0), CONSTRAINT "UQ_email" UNIQUE ("email"), CONSTRAINT "UQ_username" UNIQUE ("username"))`);
        await queryRunner.query(`INSERT INTO "user"("id", "createdAt", "updatedAt", "username", "email", "password", "isAdmin") SELECT "id", "createdAt", "updatedAt", "username", "email", "password", "isAdmin" FROM "temporary_user"`);
        await queryRunner.query(`DROP TABLE "temporary_user"`);
    }

}
