'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { injectable } from 'inversify'
import Achat from '../entity/Achat'
import Consommation from '../entity/Consommation'
import User from '../entity/User'

@injectable()
export default class CleanObjectService {
    public user(user: User): any {
        return this.cleanWithKeepAttr(user, ['id', 'username', 'email', 'isAdmin', 'stock'])
    }

    public achatWithoutUser(achat: Achat): any {
        return {id: achat.id, quantite: achat.quantite, prix: achat.prix, dateAchat: this.cleanDate(achat.dateAchat)}
    }

    public achatsWithoutUser(achats: Achat[]): any {
        return achats.map(achat => this.achatWithoutUser(achat))
    }

    public consommationWithoutUser(consommation: Consommation): any {
        return {id: consommation.id, dateConsommation: this.cleanDate(consommation.dateConsommation)}
    }

    public consommationsWithoutUser(consommations: Consommation[]): any {
        return consommations.map(consommation => this.consommationWithoutUser(consommation))
    }

    private cleanWithKeepAttr(objSrc: any, attrToKepp: string[]): any {
        const objClean: any = objSrc
        for (const [key, value] of Object.entries(objSrc)) {
            if (!attrToKepp.includes(key)) {
                delete objClean[key]
            }
        }

        return objClean
    }

    private cleanWithDelAttr(objSrc: any, attrToDelete: string[]): any {
        const objClean: any = objSrc
        for (const [key, value] of Object.entries(objSrc)) {
            if (attrToDelete.includes(key)) {
                delete objClean[key]
            }
        }

        return objClean
    }

    private cleanDate(date: string|Date): string {
        if (date instanceof Date) {
            const mm = date.getMonth() + 1
            const dd = date.getDay()
    
            return [date.getFullYear(), (mm>9 ? '' : '0') + mm, (dd>9 ? '' : '0') + dd].join('-')
        }

        return date.split('T')[0]
    }
}