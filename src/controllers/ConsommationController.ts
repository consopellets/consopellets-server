'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { inject, injectable } from 'inversify'
import { Authorized, BadRequestError, Body, CurrentUser, Get, HttpCode, InternalServerError, JsonController, NotFoundError, Post } from 'routing-controllers'
import { OpenAPI } from 'routing-controllers-openapi'
import TYPES from '../di/types'
import Consommation from '../entity/Consommation'
import I18n from '../interfaces/I18nInterface'
import Logger from '../interfaces/LoggerInterface'
import CleanObjectService from '../services/CleanObjectService'
import ConsommationService, { StockEmpty } from '../services/ConsommationService'
import { UserNotFound } from '../services/UserService'

 @OpenAPI({
    tags: ['Consommations'],
    responses: {
        '401': {
            description: "L'utilisateur n'est pas autorisé",
            content: {'application/json': {schema: {$ref: "#/components/schemas/UnauthorizedUser"}}}
        },
        '500': {
            description: "**Erreur** : problème serveur",
            content: {'application/json': {schema: {$ref: "#/components/schemas/Error"}}}
        }
    }
})
@injectable()
@JsonController('/v0.1')
export default class ConsommationController {
    @inject(ConsommationService) private readonly consommationService!: ConsommationService
    @inject(CleanObjectService) private readonly cleanObjectService! : CleanObjectService
    @inject(TYPES.I18n) private readonly i18n!: I18n
    @inject(TYPES.Logger) private readonly logger!: Logger

    @OpenAPI({
        summary: "Enregistrement consommation",
        description: "Enregistrement d'une consommation d'un sac de pellets",
        requestBody: {
            require: true,
            description: "Les données nécessaires à l'enregistrement dune consommation",
            content: {'application/json': {schema: {$ref: '#/components/schemas/ConsommationAdd'}}}
        },
        responses: {
            '201': {
                description: "La consommation a bien été créé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/Consommation"}}}
            },
            '400': {
                description: "**Erreur** : données invalides",
                content: {'application/json': {schema: {$ref: "#/components/schemas/DataError"}}}
            },
            '401': {
                description: "L'utilisateur n'est pas autorisé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/UnauthorizedUser"}}}
            }
        }
    })
    @Authorized()
    @HttpCode(201)
    @Post('/users/me/consommations')
    public async addConsommationToMe(@CurrentUser({ required: true }) userJwt: any, @Body({ validate: {groups: ['add']} }) consommation: Consommation) {
        try {
            return this.cleanObjectService.consommationWithoutUser(await this.consommationService.addConsommation(consommation, userJwt.userId))
        } catch (err:any) {
            if (err instanceof UserNotFound) {
                throw new NotFoundError(this.i18n.t('error.users.notFound'))
            }

            if (err instanceof StockEmpty) {
                throw new BadRequestError(this.i18n.t('error.users.stockEmpty'))
            }

            this.logger.error(err)
            throw new InternalServerError(this.i18n.t('error.500'))
        }
    }

    @OpenAPI({
        summary: "Récupération de mes consommations",
        description: "Récupération de mes consommations de sacs de pellets",
        security: [{'accessToken': []}],
        responses: {
            '200': {
                description: "Les consommations de l'utilisateur",
                content: {'application/json': {schema: {$ref: "#/components/schemas/Consommations"}}}
            }
        }
    })
    @Authorized()
    @Get('/users/me/consommations')
    public async getMyConsommations(@CurrentUser({ required: true }) userJwt: any) {
        try {
            const consommations = await this.consommationService.getConsommationsOfUser(userJwt.userId)
            return this.cleanObjectService.consommationsWithoutUser(consommations)
        } catch (err: any) {
            this.logger.error(err)
            throw new InternalServerError(this.i18n.t('error.500'))
        }
    }
}