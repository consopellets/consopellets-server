import {
    ValidationOptions,
    IsNotEmpty as _IsNotEmpty,
    IsInt as _IsInt,
    IsDate as _IsDate,
    Length as _Length,
    MinLength as _MinLength,
    MaxLength as _MaxLength,
    Min as _Min,
    Max as _Max,
    IsEmail as _IsEmail,
    Matches as _Matches
} from 'class-validator'
import ValidatorJS from 'validator'
// Mauvais système de chargement de i18n sans utilisation du container, mais ca fonctionne
// car quand on passe ici le container n'est pas encore chargé
import i18n from '../services/i18nService'

//lookup existing message interpolation patterns in the source:
//https://github.com/typestack/class-validator/blob/develop/src/decorator/number/Max.ts
// common
export const IsNotEmpty = (validationOptions?: ValidationOptions): PropertyDecorator =>
    _IsNotEmpty({...validationOptions, message: i18n.t('validator.common.isNotEmpty')})
// type checker
export const IsInt = (validationOptions?: ValidationOptions): PropertyDecorator =>
    _IsInt({...validationOptions, message: i18n.t('validator.typeChecker.isInt')})
export const IsDate = (validationOptions?: ValidationOptions): PropertyDecorator =>
    _IsDate({...validationOptions, message: i18n.t('validator.typeChecker.isDate')})
    // string
export const Length = (min: number, max: number, validationOptions?: ValidationOptions): PropertyDecorator =>
    _Length(min, max, {...validationOptions, message: i18n.t('validator.string.length') })
export const IsEmail =  (options?: ValidatorJS.IsEmailOptions, validationOptions?: ValidationOptions): PropertyDecorator =>
    _IsEmail(options, {...validationOptions, message: i18n.t('validator.string.isEmail')})
export const Matches = (pattern: RegExp, validationOptions?: ValidationOptions): PropertyDecorator =>
    _Matches(pattern, {...validationOptions, message: i18n.t('validator.string.matches')})
export const MinOneMajChar = (validationOptions?: ValidationOptions): PropertyDecorator =>
    _Matches(/[A-Z]+/, {...validationOptions, message: i18n.t('validator.string.minOneMajChar')})
export const MinOneMinChar = (validationOptions?: ValidationOptions): PropertyDecorator =>
    _Matches(/[a-z]+/, {...validationOptions, message: i18n.t('validator.string.minOneMinChar')})
export const MinOneSpecialChar = (validationOptions?: ValidationOptions): PropertyDecorator =>
    _Matches(/[^A-Za-z0-9]+/, {...validationOptions, message: i18n.t('validator.string.minOneSpecialChar')})
export const MinOneNum = (validationOptions?: ValidationOptions): PropertyDecorator =>
    _Matches(/[0-9]+/, {...validationOptions, message: i18n.t('validator.string.minOneNum')})
export const MaxLength = (max: number, validationOptions?: ValidationOptions): PropertyDecorator =>
    _MaxLength(max, {...validationOptions, message: i18n.t('validator.string.maxLength')})
export const MinLength = (max: number, validationOptions?: ValidationOptions): PropertyDecorator =>
    _MinLength(max, {...validationOptions, message: i18n.t('validator.string.minLength')})
/*export const Min = (minValue: number, validationOptions?: ValidationOptions): PropertyDecorator =>
    _Min(minValue, {...validationOptions, message: ">= $constraint1"})
export const Max = (maxValue: number, validationOptions?: ValidationOptions): PropertyDecorator =>
    _Max(maxValue, {...validationOptions, message: `$constraint1 max`})
*/