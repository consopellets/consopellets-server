'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import container from './di/container'
import express, { NextFunction, Request, Response } from 'express'
import config from 'config'
import UserController from './controllers/UserController'
import { Action, getMetadataArgsStorage, HttpError, RoutingControllersOptions, useExpressServer } from 'routing-controllers'
import cors from 'cors'
import TYPES from './di/types'
import confOpenAPI from './utils/swaggerJsdocUtil'
import swaggerUI from 'swagger-ui-express'
import I18n from './interfaces/I18nInterface'
import AuthController from './controllers/AuthController'
import { routingControllersToSpec } from 'routing-controllers-openapi'
import SecuritySchemeType from './../node_modules/openapi3-ts/dist/model/OpenApi.d'
import JwtService from './services/JwtService'
import AchatController from './controllers/AchatController'
import ConsommationController from './controllers/ConsommationController'

const i18n: I18n = container.get(TYPES.I18n)
const app = express()


const routingControllersOptions = {
    routePrefix: '/api',
    controllers: [
        AuthController,
        UserController,
        AchatController,
        ConsommationController,
    ],
    development: config.get('debug.showStackTraceInServerReponse'),
    //defaultErrorHandler: false,
    authorizationChecker: async (action: Action, roles: string[]) => {
        try {
            JwtService.verifyAccessToken(action.request)
        } catch(err) {
            return false
        }

        return true
    },
    currentUserChecker: async (action: Action) => action.request.user
} as RoutingControllersOptions

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())
useExpressServer(app, routingControllersOptions)
const spec = routingControllersToSpec(getMetadataArgsStorage(), routingControllersOptions, confOpenAPI as Partial<SecuritySchemeType.OpenAPIObject>)
// Suppression des données récoltés en trop
delete spec.paths['/api/v0.1/auth/refreshToken/'].post.parameters

app.use('/api-docs/', swaggerUI.serve, swaggerUI.setup(spec))

// Traitement des erreurs
app.use((err: Error, req: Request, res: Response, next: NextFunction): Response|void => {
    if (res.headersSent) {
        return
    }

    // Gestion des erreurs de jwt
    if (err.name === 'UnauthorizedError' || err.name === 'AuthorizationRequiredError') {
        return res.status(401).json({name:'Unauthorized', message: i18n.t('error.jwtToken.unauthorized')})
    }

    if (err instanceof HttpError) {
        return res.status(err.httpCode).json({name: err.name, message: err.message})
    }

    return res.status(500).json({name: "InternalServerError", message: i18n.t('error.500')})
})

// Si rien n'a été retourné jusque là, c'est que l'on est en 404
app.use((req: Request, res: Response, next: NextFunction): Response|void => {
    if (!res.headersSent) {
        return res.status(404).json({name:'NotFoundError', message: i18n.t('error.404')})
    }
})

export default app