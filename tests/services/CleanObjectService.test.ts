'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import 'reflect-metadata'
import CleanObjectService from '../../src/services/CleanObjectService'
import User from '../../src/entity/User'

describe("Test de la méthode user de la class CleanObjectService", () => {
    const datas = [
        {src: {id:0, username:1, email:2, other:3, isAdmin:4}, res:{id:0, username:1, email:2, isAdmin:4}},
        {src: {foo:0, id:1, bar:2}, res: {id:1}},
        {src: {foo:0, bar:1}, res: {}}
    ]
    datas.forEach(data => {
        test('Ne doit retourner que les attributs bon attributs', () => {
            const cleanObjectService = new CleanObjectService()
            const src = cleanObjectService.user(data.src as unknown as User)
            expect(src).toEqual(data.res)
            expect(data.res).toEqual(src)
        })
    })
})
