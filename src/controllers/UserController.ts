'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { Authorized, Body, CurrentUser, Get, HttpCode, InternalServerError, JsonController, NotFoundError, OnUndefined, Param, Patch, Post, UnauthorizedError } from 'routing-controllers'
import { inject, injectable } from 'inversify'
import User from '../entity/User'
import UserService, { UserNotFound } from '../services/UserService'
import Logger from '../interfaces/LoggerInterface'
import TYPES from '../di/types'
import I18n from '../interfaces/I18nInterface'
import CleanObjectService from '../services/CleanObjectService'
import { validate } from 'class-validator'
import { ConflictError } from '../errors/ConflictError'
import { OpenAPI } from 'routing-controllers-openapi'
import JwtService from '../services/JwtService'

@OpenAPI({
    tags: ['Users'],
    responses: {
        '500': {
            description: "**Erreur** : problème serveur",
            content: {'application/json': {schema: {$ref: "#/components/schemas/Error"}}}
        }
    }
})
@injectable()
@JsonController('/v0.1/users')
export default class UserController {
    @inject(UserService) private readonly userService!: UserService
    @inject(CleanObjectService) private readonly cleanObjectService! : CleanObjectService
    @inject(TYPES.I18n) private readonly i18n!: I18n
    @inject(TYPES.Logger) private readonly logger!: Logger

    @OpenAPI({
        summary: "Enregistrement utilisateur",
        description: "Enregistrement d'un utilisateur",
        requestBody: {
            require: true,
            description: "Les données nécessaires à l'enregistrement d'un utilisateur",
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/UserRegister'
                    }
                }
            }
        },
        responses: {
            '201': {
                description: "L'utilisateur a bien été créé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/User"}}}
            },
            '400': {
                description: "**Erreur** : données invalides",
                content: {'application/json': {schema: {$ref: "#/components/schemas/DataError"}}}
            },
            '409': {
                description: "**Erreur** : conflit de données",
                content: {'application/json': {schema: {$ref: "#/components/schemas/DataError"}}}
            },
        }
    })
    @HttpCode(201)
    @Post('/')
    public async register(@Body({ validate: {groups: ['register']} }) user: User) {
        try {
            const errors = await validate(user, {groups: ['register-conflict']})

            if (errors.length > 0) {
                throw new ConflictError(errors)
            }

            return this.cleanObjectService.user(await this.userService.register(user))
        } catch (err:any) {
            if (err instanceof ConflictError) {
                throw err
            } else {
                this.logger.error(err)
                throw new InternalServerError(this.i18n.t('error.500'))
            }
        }
    }

    @OpenAPI({
        summary: "Récupération de mon utilisateur",
        description: "Récupération de mon utilisateur",
        security: [{'accessToken': []}],
        responses: {
            '200': {
                description: "Les données de l'utilisateur",
                content: {'application/json': {schema: {$ref: "#/components/schemas/User"}}}
            },
            '401': {
                description: "L'utilisateur n'est pas autorisé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/UnauthorizedUser"}}}
            }
        }
    })
    @Authorized()
    @Get('/me/')
    public async getMe(@CurrentUser({ required: true }) userJwt: any) {
        return this.getUser(userJwt.userId)
    }

    @OpenAPI({
        summary: "Récupération d'un utilisateur",
        description: "Récupération d'un utilisateur",
        security: [{'accessToken': []}],
        responses: {
            '200': {
                description: "Les données de l'utilisateur",
                content: {'application/json': {schema: {$ref: "#/components/schemas/User"}}}
            },
            '401': {
                description: "L'utilisateur n'est pas autorisé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/UnauthorizedUser"}}}
            }
        }
    })
    @Authorized()
    @Get('/:id')
    public async getOne(@CurrentUser({ required: true }) userJwt: any, @Param('id') id: number) {
        if (userJwt.userId !== id) {
            throw new UnauthorizedError(this.i18n.t('error.users.consultRight'))
        }

        return this.getUser(userJwt.userId)
    }

    @OpenAPI({
        summary: "Mise à jour utilisateur",
        description: "Mise à jour des données utilisateur",
        security: [{'accessToken': []}],
        requestBody: {
            require: true,
            description: "Les données nécessaires à la mise à jour d'un utilisateur",
            content: {
                'application/json': {
                    schema: {
                        $ref: '#/components/schemas/UserUpdate'
                    }
                }
            }
        },
        responses: {
            '204': {
                description: "L'utilisateur a bien été mis à jour"
            },
            '401': {
                description: "L'utilisateur n'est pas autorisé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/UnauthorizedUser"}}}
            }
        }
    })
    @Authorized()
    @HttpCode(204)
    @OnUndefined(204)
    @Patch('/:id')
    public async updateOne(
        @CurrentUser({ required: true }) userJwt: any,
        @Param('id') id: number,
        @Body({ validate: {groups: ['update']} }) userData: User
    ) {
        if (userJwt.userId !== id) {
            throw new UnauthorizedError(this.i18n.t('error.users.updateRight'))
        }

        try {
            const user = await this.userService.findUserByPk(id)
            user.password = userData.password
            await this.userService.updateUserPassword(user)
            return
        } catch (err: any) {
            if (err instanceof UserNotFound) {
                throw new NotFoundError(this.i18n.t('error.users.notFound'))
            }

            this.logger.error(err)
            throw new InternalServerError(this.i18n.t('error.500'))
        }
    }

    private async getUser(id: number) {
        try {
            const user = await this.userService.findUserByPk(id)

            return this.cleanObjectService.user(user)
        } catch (err: any) {
            if (err instanceof UserNotFound) {
                throw new NotFoundError(this.i18n.t('error.users.notFound'))
            }

            this.logger.error(err)
            throw new InternalServerError(this.i18n.t('error.500'))
        }
    }
}