'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import config from 'config'
import { inject, injectable } from 'inversify'
import { Connection, ConnectionOptions, createConnection, ObjectType } from 'typeorm'
import TYPES from '../di/types'
import Achat from '../entity/Achat'
import Consommation from '../entity/Consommation'
import Entretient from '../entity/Entretient'
import Nettoyage from '../entity/Nettoyage'
import RefreshToken from '../entity/RefreshToken'
import User from '../entity/User'
import Logger from '../interfaces/LoggerInterface'

@injectable()
export default class DatabaseService {
    @inject(TYPES.Logger) private readonly logger!: Logger
    private static connection: Connection

    public async getConnection(): Promise<Connection> {
        if (DatabaseService.connection instanceof Connection) {
            return DatabaseService.connection
        }

        const confTypeORM: ConnectionOptions = Object.assign({}, {
            entities: [
                //__dirname + '/entity/**/*.{js,ts}'
                User,
                Achat,
                Consommation,
                Entretient,
                Nettoyage,
                RefreshToken
            ]
        }, config.get('database'), config.get('typeORM')) as unknown as ConnectionOptions

        try {
            DatabaseService.connection = await createConnection(confTypeORM)
            this.logger.info("Connexion db créé")
        } catch (err: any) {
            this.logger.error(err)
            process.exit(1)
        }

        return DatabaseService.connection
    }

    public async getRepository<T>(repository: ObjectType<T>): Promise<T> {
        const connection = await this.getConnection()
        return await connection.getCustomRepository<T>(repository)
    }
}