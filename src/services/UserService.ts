'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import bcrypt from 'bcrypt'
import config from 'config'
import User from '../entity/User'
import UserRepository from '../repositories/UserRepository'
import { inject, injectable } from 'inversify'
import DatabaseService from './DatabaseService'

@injectable()
export default class UserService {
    @inject(DatabaseService) private readonly databaseService!: DatabaseService

    /**
     * Enregistre un nouvel utilisateur
     *
     * @param user Le user à enregistrer
     *
     * @returns Le user tel qu'enregistré
     */
    public async register(user: User):Promise<User> {
        const userRepo = await this.databaseService.getRepository(UserRepository)
        // On hash le password
        user.password = await bcrypt.hash(user.password, config.get('conf.passwordSaltRound'))
        // On enregistre le user
        const newUser = await userRepo.save(user)
        // On vide le champ password par sécurité
        newUser.password = ''
        // On retourne le nouveau user
        return newUser
    }

    public async isUserPasswordValidAndGetUser(user: User): Promise<{ isValidPassword: boolean, user: User}> {
        const userRepo = await this.databaseService.getRepository(UserRepository)
        // On récupère le user
        const userFromDB = await userRepo.findOneByUsernameWithPassword(user)

        // Si l'utilisateur n'a pas été trouvé
        if (undefined === userFromDB) {
            throw new UserNotFound()
        }

        // On compare les password et on retourne le resultat et le user
        return { isValidPassword: await bcrypt.compare(user.password, userFromDB.password), user: userFromDB }
    }

    public async findUserByPk(userId: number): Promise<User> {
        const userRepo = await this.databaseService.getRepository(UserRepository)
        const user = await userRepo.findByPk(userId)

        // Si l'utilisateur n'a pas été trouvé
        if (undefined === user) {
            throw new UserNotFound()
        }

        return user
    }

    public async updateUserPassword(user: User):Promise<void> {
        const userRepo = await this.databaseService.getRepository(UserRepository)
        // On hash le password
        user.password = await bcrypt.hash(user.password, config.get('conf.passwordSaltRound'))
        // On met à jour le password
        const res = await userRepo.updatePassword(user)

        if (res.affected === 1) {
            return
        } else {
            // Le mot de passe n'a pas été mis à jour
            throw new Error()
        }
    }
}

export class UserNotFound extends Error {}