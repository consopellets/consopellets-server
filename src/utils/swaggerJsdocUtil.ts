'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import config from 'config'

const pjson = require('../../package.json')
const confServer = config.get('server') as {protocol: string, hostname: string, port: number}
const url = confServer.protocol+'://'+confServer.hostname+':'+confServer.port

// TODO: a remettre au propre
const optionsInfos = {
    "title": pjson.name,
    "description": pjson.description,
    /*"termsOfService": "http://example.com/terms/",
    "contact": {
      "name": "API Support",
      "url": "http://www.example.com/support",
      "email": "support@example.com"
    },*/
    "license": {
      "name": pjson.license,
      "url": "https://www.gnu.org/licenses/agpl-3.0.fr.html"
    },
    "version": pjson.version
  }

const confOpenAPI = {
  openapi: '3.0.0',
  info: optionsInfos,
  servers: [{
    url: url,
    description: "Development server"
  }],
  tags: [{
    name: "Authentication",
    description: "Gestion de l'authentification"
  }, {
    name: "Users",
    description: "Gestion des utilisateurs"
  }, {
    name: "Achats",
    description: "Gestion des achats de sacs de pellets"
  }, {
    name: "Consommations",
    description: "Gestion des consommations de sacs de pellets"
  }],
  components: {
    securitySchemes: {
      'accessToken': {type: 'http', scheme: "bearer", bearerFormat: "JWT"},
      'refreshToken': {type: 'http', scheme: "bearer", bearerFormat: "JWT"}
    },
    schemas: {
      'TokenData': {
        type: 'object',
        properties: {
          'userId': {type: 'integer', description: "L'identifiant de l'utilisateur"},
          'accessToken': {type: 'string', description: "L'access token pour accéder à l'api"},
          'refreshToken': {type: 'string', description: "Le refresh token pour rafraîchir les tokens"},
        },
        example: {
          'userId': 1,
          'accessToken': "accessToken",
          'refreshToken': "refreshToken"
        }
      },
      'DataError': {
        type: 'object',
        properties: {
          'name': {type: 'string', description: "Le nom de l'erreur"},
          'message': {type: 'string', description: "Le message d'erreur"},
          'errors': {
            type: 'array',
            description: "Le tableau des différentes erreurs",
            items: {
              type: 'object',
              properties: {
                target: {type: 'object', description: "L'objet sur lequel il y'a une erreur"},
                property: {type: 'string', description: "La propriété qui rencontre une erreur"},
                // TODO: à compléter
                children: {type: 'array', description: "A définir"},
                constraints: {type: 'object', description: "Les différentes contraintes qui sont en erreur avec leur message d'erreur"}
              }
            }
          },
        },
        example: {
          'name': 'BadRequestError',
          'message': "Invalid body, check 'errors' property for more info.",
          'errors': [{
            target: {'username': "JohnDoe", 'password': "DoeJohn1", 'email': ""},
            property: 'email',
            children: [],
            constraints: {'isNotEmpty': "email ne doit pas être vide"}
          }]
        }
      },
      'Error': {
        type: 'object',
        properties: {
          'name': {type: 'string', description: "Indique le nom de l'erreur"},
          'message': {type: 'string', description: "Le message d'erreur"}
        },
        example: {name: "InternalServerError", message: "Erreur serveur"}
      },
      'UnauthorizedUser': {
        type: 'object',
        properties: {
          'name': {type: 'string', description: "Indique le nom de l'erreur"},
          'message': {type: 'string', description: "Le message d'erreur"}
        },
        example: {name: "Unauthorized", message: "Non autorisé, votre token d'authentification est invalide"}
      },
      'UserRegister': {
        type: 'object',
        properties: {
          'username': {type: 'string', description: "Le nom d'utilisateur"},
          'email': {type: 'string', description: "L'email de l'utilisateur"},
          'password': {type: 'string', description: "Le mot de passe de l'utilisateur"}
        },
        example: {'username': "JohnDoe", 'email': "john@doe.fr", 'password': "JohnDoe1#"}
      },
      'User': {
        type: 'object',
        properties: {
            'id': {type: 'integer', description: "L'identifiant de l'utilisateur"},
            'username': {type: 'string', description: "Le nom d'utilisateur"},
            'email': {type: 'string', description: "L'email de l'utilisateur"},
            'isAdmin': {type: 'boolean', description: "Indique si l'utilisateur est admin"},
            'stock': {type: 'integer', description: "Le nombre de sacs de pellets restant en stock"}
        },
        example: {'id': 1, 'username': "JohnDoe", 'email': "john@doe.fr", 'is-admin': false}
      },
      'UserLogin': {
        type: 'object',
        properties: {
          'username': {type: 'string', description: "Le nom d'utilisateur"},
          'password': {type: 'string', description: "Le mot de passe de l'utilisateur"}
        },
        example: {'username': "JohnDoe", 'password': "JohnDoe1#"}
      },
      'UserUpdate': {
        type: 'object',
        properties: {
          'password': {type: 'string', description: "Le mot de passe de l'utilisateur"}
        },
        example: {'password': "JohnDoe1#"}
      },
      'Achat': {
        type: 'object',
        properties: {
            'id': {type: 'integer', description: "L'identifiant de l'achat"},
            'quantite': {type: 'integer', description: "Le nombre de sacs de pellets"},
            'prix': {type: 'integer', description: "Le prix unitaire d'un sac de pellets en centimes"},
            'dateAchat': {type: 'string', format: 'date', description: "La date de l'achat"}
        },
        example: {'id': 1, 'quantite': 10, 'prix': 350, 'dateAchat': "2021-09-30"}
      },
      'Achats': {
        type: 'array',
        description: "Le tableau des achats",
        items: {
          type: 'object',
          properties: {
              'id': {type: 'integer', description: "L'identifiant de l'achat"},
              'quantite': {type: 'integer', description: "Le nombre de sacs de pellets"},
              'prix': {type: 'integer', description: "Le prix unitaire d'un sac de pellets en centimes"},
              'dateAchat': {type: 'string', format: 'date', description: "La date de l'achat"}
          },
        },
        example: [{'id': 1, 'quantite': 10, 'prix': 350, 'dateAchat': "2021-09-30"}]
      },
      'AchatAdd': {
        type: 'object',
        properties: {
            'quantite': {type: 'integer', description: "Le nombre de sacs de pellets"},
            'prix': {type: 'integer', description: "Le prix unitaire d'un sac de pellets en centimes"},
            'dateAchat': {type: 'string', format: 'date', description: "La date de l'achat"}
        },
        example: {'quantite': 10, 'prix': 350, 'dateAchat': "2021-09-30"}
      },
      'Consommation': {
        type: 'object',
        properties: {
            'id': {type: 'integer', description: "L'identifiant de la consommation"},
            'dateConsommation': {type: 'string', format: 'date', description: "La date de la consommation"}
        },
        example: {'id': 1, 'dateConsommation': "2021-09-30"}
      },
      'Consommations': {
        type: 'array',
        description: "Le tableau des consommations",
        items: {
          type: 'object',
          properties: {
              'id': {type: 'integer', description: "L'identifiant de la consommation"},
              'dateConsommation': {type: 'string', format: 'date', description: "La date de la consommation"}
          },
        },
        example: [{'id': 1, 'dateConsommation': "2021-09-30"}]
      },
      'ConsommationAdd': {
        type: 'object',
        properties: {
            'dateConsommation': {type: 'string', format: 'date', description: "La date de consommation d'un sac de pellets"}
        },
        example: {'dateConsommation': "2021-09-30"}
      }
    }
  }
}

export default confOpenAPI