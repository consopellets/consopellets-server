'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { BadRequestError, Body, ForbiddenError, HeaderParam, InternalServerError, JsonController, NotFoundError, Post, UseBefore } from 'routing-controllers'
import { inject, injectable } from 'inversify'
import User from '../entity/User'
import UserService, { UserNotFound } from '../services/UserService'
import Logger from '../interfaces/LoggerInterface'
import TYPES from '../di/types'
import I18n from '../interfaces/I18nInterface'
import CleanObjectService from '../services/CleanObjectService'
import JwtService, { RecycledRefreshTokenError } from '../services/JwtService'
import { OpenAPI } from 'routing-controllers-openapi'

@OpenAPI({
    tags: ['Authentication'],
    responses: {
        '500': {
            description: "**Erreur** : problème serveur",
            content: {'application/json': {schema: {$ref: "#/components/schemas/Error"}}}
        }
    }
})
@injectable()
@JsonController('/v0.1/auth')
export default class AuthController {
    @inject(UserService) private readonly userService!: UserService
    @inject(JwtService) private readonly jwtService!: JwtService
    @inject(CleanObjectService) private readonly cleanObjectService! : CleanObjectService
    @inject(TYPES.I18n) private readonly i18n!: I18n
    @inject(TYPES.Logger) private readonly logger!: Logger

    @OpenAPI({
        summary: "Connexion utilisateur",
        description: "Connexion de l'utilisateur",
        requestBody: {
            require: true,
            description: "Les données nécessaires a la connexion d'un utilisateur",
            content: {'application/json': {schema: {$ref: '#/components/schemas/UserLogin'}}}
        },
        responses: {
            '200': {
                description: "L'utilisateur est bien connecté",
                content: {'application/json': {schema: {$ref: "#/components/schemas/TokenData"}}}
            },
            '403': {
                description: "**Erreur** : données invalides",
                content: {'application/json': {schema: {$ref: "#/components/schemas/DataError"}}}
            }
        }
    })
    @Post('/login/')
    public async login(@Body({ validate: {groups: ['login']} }) userSrc: User): Promise<any> {
        try {
            const { isValidPassword, user} = await this.userService.isUserPasswordValidAndGetUser(userSrc)

            // Si le password n'est pas valide on lance une exception
            if (!isValidPassword) {
                throw new ForbiddenError()
            }

            // On génère un refresh token
            const refreshToken = await this.jwtService.generateRefreshToken(user)

            // Et on retourne refreshToken et accessToken
            return {
                userId: user.id,
                accessToken: this.jwtService.generateAccessToken(user),
                refreshToken: refreshToken
            }
        } catch (err: any) {
            if (err instanceof UserNotFound || err instanceof ForbiddenError) {
                throw new ForbiddenError(this.i18n.t('error.usernameOrPasswordInvalid'))
            } else {
                this.logger.error(err)
                throw new InternalServerError(this.i18n.t('error.500'))
            }
        }
    }

    // Permet de vérifier que le refreshToken est bien ok est sort en exception si ce n'est pas le cas
    @OpenAPI({
        summary: "Rafraîchissement des tokens",
        description: "Rafraîchissement des tokens",
        security: [{'refreshToken': []}],
        responses: {
            '200': {
                description: "L'utilisateur est bien connecté",
                content: {'application/json': {schema: {$ref: "#/components/schemas/TokenData"}}}
            },
            '401': {
                description: "L'utilisateur n'est pas autorisé",
                content: {'application/json': {schema: {$ref: "#/components/schemas/UnauthorizedUser"}}}
            },
            '403': {
                description: "**Erreur** : données invalides",
                content: {'application/json': {schema: {$ref: "#/components/schemas/DataError"}}}
            }
        }
    })
    @UseBefore(JwtService.verifyRefreshTokenHandler())
    @Post('/refreshToken/')
    public async refreshToken(@HeaderParam("authorization") authorization: string): Promise<any> {
        try {
            const refreshToken = authorization.split(' ')[1]
            const { userId } = await this.jwtService.decodeRefreshToken(refreshToken) as {userId: number}
            const user = await this.userService.findUserByPk(userId)
            // Ici on génère un nouveau refresh token, mais en même temps on vérifi que l'ancien n'a pas été invalidé
            //  et si il est invalidé on déclenche une exception RecycledRefreshTokenError
            const newRefreshToken = await this.jwtService.generateRefreshToken(user, refreshToken)

            // Et on retourne refreshToken et accessToken
            return {
                userId: user.id,
                accessToken: this.jwtService.generateAccessToken(user),
                refreshToken: newRefreshToken
            }
        } catch (err: any) {
            if (err instanceof UserNotFound) {
                throw new NotFoundError(this.i18n.t('error.404.user'))
            } else if (err instanceof RecycledRefreshTokenError) {
                throw new BadRequestError(this.i18n.t('error.refreshToken.recycled'))
            } else {
                this.logger.error(err)
                throw new InternalServerError(this.i18n.t('error.500'))
            }
        }
    }
}