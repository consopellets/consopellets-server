'use strict'
import { Type } from 'class-transformer'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { Column, Entity, ManyToOne } from 'typeorm'
import { IsDate, IsInt, IsNotEmpty } from '../validators/validators'
import MainEntity from './MainEntity'
import User from './User'

@Entity()
export default class Achat extends MainEntity {
    @Type(() => User)
    @ManyToOne(type => User, user => user.achats, {nullable: false, onDelete:'CASCADE'})
    public user!: User

    @Type(() => Number)
    @IsInt({groups: ['add']})
    @IsNotEmpty({groups: ['add']})
    @Column({
        type: 'integer',
        nullable: false
    })
    public quantite!: number

    @Type(() => Number)
    @IsInt({groups: ['add']})
    @IsNotEmpty({groups: ['add']})
    @Column({
        type: 'integer',
        nullable: false
    })
    public prix!: number

    @Type(() => Date)
    @IsDate({groups: ['add']})
    @IsNotEmpty({groups: ['add']})
    @Column({
        type: 'date',
        nullable: false
    })
    public dateAchat!: Date
}