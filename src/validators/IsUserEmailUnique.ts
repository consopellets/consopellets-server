'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator'
import User from '../entity/User'
import UserRepository from '../repositories/UserRepository'
import DatabaseService from '../services/DatabaseService'
import container from '../di/container'
import I18n from '../interfaces/I18nInterface'
import TYPES from '../di/types'

@ValidatorConstraint({ name: 'IsUserEmailUnique', async: true })
export default class IsUserEmailUnique implements ValidatorConstraintInterface {
    private readonly databaseService!: DatabaseService
    private readonly i18n!: I18n

    constructor() {
        this.databaseService = container.get(DatabaseService)
        this.i18n = container.get(TYPES.I18n)
    }

    public async validate(email: string, args: ValidationArguments): Promise<boolean> {
        const userRepo = await this.databaseService.getRepository(UserRepository)

        return await userRepo.createQueryBuilder('user')
            .where('user.email = :email', {email})
            .getOne()
            .then((userFound: User|undefined):boolean => {
                return userFound === undefined
            })
    }

    public defaultMessage(args: ValidationArguments): string {
        return this.i18n.t('validator.custom.IsUserEmailUnique')
    }
}