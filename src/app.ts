'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
// reflect-metadata a placer au tout début du premier fichier
import 'reflect-metadata'
import container from './di/container'
import config from 'config'
import Logger from './interfaces/LoggerInterface'
import TYPES from './di/types'
import app from './index'

const logger: Logger = container.get(TYPES.Logger)
const port = config.get('server.port')

app.listen(port, () => {
    logger.info(`Server started : http://localhost:${port} in ${process.env.NODE_ENV} ENV`)
})