'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { Container } from 'inversify'
import { useContainer as useContainerRouting } from 'routing-controllers'
import TYPES from './types'
import Logger from '../interfaces/LoggerInterface'
import I18n from '../interfaces/I18nInterface'
import logger from '../services/loggerService'
import i18n from '../services/i18nService'
// déclenche le chargement de la class contenant les validateurs
import UserService from '../services/UserService'
import UserController from '../controllers/UserController'
import DatabaseService from '../services/DatabaseService'
import CleanObjectService from '../services/CleanObjectService'
import JwtService from '../services/JwtService'
import AuthController from '../controllers/AuthController'
import AchatService from '../services/AchatService'
import AchatController from '../controllers/AchatController'
import ConsommationController from '../controllers/ConsommationController'
import ConsommationService from '../services/ConsommationService'

const container = new Container()
container.bind<Logger>(TYPES.Logger).toConstantValue(logger)
container.bind<I18n>(TYPES.I18n).toConstantValue(i18n)
container.bind<DatabaseService>(DatabaseService).toSelf().inSingletonScope()
container.bind<JwtService>(JwtService).toSelf().inSingletonScope()
container.bind<CleanObjectService>(CleanObjectService).toSelf().inSingletonScope()

container.bind<UserService>(UserService).toSelf().inSingletonScope()
container.bind<AchatService>(AchatService).toSelf().inSingletonScope()
container.bind<ConsommationService>(ConsommationService).toSelf().inSingletonScope()

container.bind<AuthController>(AuthController).toSelf().inSingletonScope()
container.bind<UserController>(UserController).toSelf().inSingletonScope()
container.bind<AchatController>(AchatController).toSelf().inSingletonScope()
container.bind<ConsommationController>(ConsommationController).toSelf().inSingletonScope()


// Méthode propre pour utiliser l'injection de dépendance sur les validator custom
//  Hélas une fois activé, on ne récupére que des erreurs http.
//  Du coup, pour contourner j'utilise le container dans les custom validator
//  pour charger mes services.
/*decorate(injectable(), Validator)
class InversifyAdapter implements IocAdapter {
    constructor(private readonly container: Container) {}
  
    get<T>(someClass: ClassConstructor<T>, action?: Action): T {
      const childContainer = this.container.createChild()
      // Les custom validator a enregistrer
      childContainer.bind<IsUserUsernameUnique>(IsUserUsernameUnique).toSelf()
      return childContainer.resolve<T>(someClass)
    }
}
useContainerClassValidator(new InversifyAdapter(container))*/

useContainerRouting(container)

export default container