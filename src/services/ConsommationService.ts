'use strict'
/**
 * @copyright Copyright (c) 2021
 *
 * @author Loïc Villanné <l.villanne@gmail.com>
 *
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
import { inject, injectable } from 'inversify'
import Consommation from '../entity/Consommation'
import ConsommationRepository from '../repositories/ConsommationRepository'
import UserRepository from '../repositories/UserRepository'
import DatabaseService from './DatabaseService'
import { UserNotFound } from './UserService'

@injectable()
export default class ConsommationService {
    @inject(DatabaseService) private readonly databaseService!: DatabaseService

    public async getConsommationsOfUser(userId: number): Promise<Consommation[]> {
        const consommationRepo = await this.databaseService.getRepository(ConsommationRepository)
        return await consommationRepo.getConsommationsOfUser(userId)
    }

    public async addConsommation(consommation: Consommation, userId: number): Promise<Consommation> {
        const userRepo = await this.databaseService.getRepository(UserRepository)
        const consommationRepo = await this.databaseService.getRepository(ConsommationRepository)
        const user = await userRepo.findByPk(userId)

        // Si l'utilisateur n'a pas été trouvé
        if (undefined === user) {
            throw new UserNotFound()
        }

        if (user.stock === 0) {
            throw new StockEmpty()
        }

        const dbConnection = await this.databaseService.getConnection()
        consommation.user = user

        return dbConnection.transaction(async transactionalEntityManager => {
            const transactionUserRepo = transactionalEntityManager.getCustomRepository(UserRepository)
            // On met à jour le stock
            transactionUserRepo.decrementStock(user)
            // On enregistre la consommation et on la retourne
            return consommationRepo.save(consommation)
        })
    }
}

export class StockEmpty extends Error {}